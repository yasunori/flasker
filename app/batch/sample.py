# -*- coding: utf-8 -*-    
import sys                 
import os                  
sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../../')

def main(app):
    from app.model.users import Users 
    from app.model.usersettings import UserSettings
    user = Users(name='test', shop_name='店舗', password='12345678', mail_address='test@test.jp')
    user_setting = UserSettings(type=1)
    user.user_setting = user_setting
    user.manager.add(user)
    user.manager.commit()

if __name__ == '__main__':
    import config
    app = config.create_app(webapp=False)
    main(app)
