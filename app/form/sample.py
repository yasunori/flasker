from wtforms import TextField, IntegerField, BooleanField, TextAreaField, HiddenField, SelectMultipleField, SelectField, RadioField, PasswordField, FileField
from wtforms import validators, ValidationError
from app.lib.form import WebForm, CriticalValidationError
from app.lib import master

def check_valid_user(form, field):
    print(form.mail_address.data)
    #ValidationError('ショップが見つかりません')

class UserLoginForm(WebForm):

    def after_init(self, formdata, obj, data, init_data, **kwargs):
        self.test_type.choices = self.create_choices(master.get('test_type'))

    
    mail_address = TextField('メールアドレス', [ 
        validators.Required(message="メールアドレスを入力してください"),
        validators.Length(max=50, message="50文字以内で入力してください")])

    password = PasswordField('パスワード', [ 
        validators.Required(message="パスワードを入力してください"),
        check_valid_user]) 

    test_type = RadioField(label='送信方法', validators=[
        validators.Required(message="送信方法を入力してください")], coerce=int)
