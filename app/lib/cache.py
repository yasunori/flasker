import logging
import traceback   
from werkzeug.contrib.cache import MemcachedCache
from config import setting

__cache = MemcachedCache([setting['memcached_host'] + ':' + setting['memcached_port']])

def set(key, value, time=300):
    try:
        return __cache.set(key, value, time)
    except Exception as e:
        logging.error(traceback.format_exc())
        return None

def get(key):
    try:
        return __cache.get(key)
    except Exception as e:
        logging.error(traceback.format_exc())
        return None
