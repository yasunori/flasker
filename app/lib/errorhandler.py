# -*- coding: utf-8 -*-
from flask import render_template


def set_error_handler(app):
    @app.errorhandler(404)
    def page_not_found(error):
        return render_template('common/error.haml'), 404 

    @app.errorhandler(500)
    def internal_server_error(error):
        return render_template('common/error.haml'), 500 
