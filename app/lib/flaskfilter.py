
# -*- coding: utf-8 -*-
from flask import request, g
from app.lib import initmessage, master
from app.model import close_db


def set_filters(app):
    @app.before_request
    def init_app_dict():
        '''
        グローバルな辞書を定義する。表示するものを格納
        '''
        g.dt = {}

    @app.before_request
    def init_cookie_array():
        '''
        クッキーを最後に焼くため、空の配列を用意しておく
        '''
        g.cookies = []


    @app.before_request
    def init_message():
        '''
        ポップアップで一度だけ表示するメッセージ。
        あれば、グローバルオブジェクトに入れておく。
        これは、テンプレートで表示することを期待。
        '''
        if '/static/' not in request.path:
            init_message = initmessage.get()
            if init_message:
                g.init_message = init_message

    @app.before_request
    def set_meta():
        '''
        htmlメタ情報をセットする
        '''
        if '/static/' not in request.path:
            page_id = None
            if request.path.endswith('/') or request.path.endswith('.html') or request.path.endswith('.htm'):  # 普通のURL
                path = request.path
            else:
                if '/page/' in request.path:  # ページング情報があった
                    pointer = -1
                    page_id = request.path.split('/')[-1]
                else:
                    pointer = -1
                path = '/'.join(request.path.split('/')[:pointer]) + '/'

            meta = master.get_meta(path)
            if not meta:
                meta = master.get_meta('default')

            if page_id:
                meta = dict([(k, v.replace('@@page_id@@', page_id)) for k, v in meta.items()])

            g.meta = meta


    @app.after_request
    def bake_cookie(response):
        if g.get('cookies'):
            for cookie in g.get('cookies'):
                import datetime as dt
                now = dt.datetime.now()
                current_time = int(now.strftime('%s'))
                exp = cookie['limit'] + current_time
                response.set_cookie(key=cookie['key'], value=cookie['value'], expires=exp)
        return response

    @app.teardown_appcontext
    def _close_db(exception=None):
        '''
        リクエスト終了時にDBのセッションを切る
        '''
        close_db()
