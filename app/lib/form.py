# -*- coding: utf-8 -*-
#from flask_wtf import Form
from wtforms import Form


class WebForm(Form):
    """
    WTFormsに便利機能を追加したもの
    """

    def _get_translations(self):
        return MyTranslations()

    @classmethod
    def create(cls, formdata=None, obj=None, prefix='', data=None, meta=None, init_data=None, **kwargs):
        """ フォームのファクトリメソッド """
        cls.before_init(formdata, obj, data, init_data, **kwargs)
        return cls(formdata, obj, prefix, data, meta, init_data, **kwargs)

    @classmethod
    def before_init(self, formdata, obj, data, init_data, **kwargs):
        """ Fieldを動的に追加する場合、この関数をオーバーライドする """
        pass

    def __init__(self, formdata=None, obj=None, prefix='', data=None, meta=None, init_data=None, **kwargs):
        """ コンストラクタ """
        super().__init__(formdata, obj, prefix, data, meta, **kwargs)
        self.after_init(formdata, obj, data, init_data, **kwargs)

    def after_init(self, formdata, obj, data, init_data, **kwargs):
        """ 選択肢やデフォルト値を動的に設定する場合、この関数をオーバーライドする """
        pass

    def validate(self):
        """ バリデート成功したら、get_easy_data を呼び出すためにオーバーライド """
        varet = super().validate()
        if varet:
            self.get_easy_data()
        return varet

    def get_easy_data(self):
        """ 人の目に分かりやすいデータを作る（確認画面とかで使う） """

        for field in self:
            if not field.data:
                continue

            if hasattr(field, 'choices'):
                # 選択肢がある場合
                data = field.data
                if not isinstance(field.data, list):
                    data = [field.data]  # 複数の場合にあわせる

                choices = [str(x[1]) for x in field.choices if x[0] in data]
                field.easy_data = ",".join(choices)

            else:
                # 選択肢がない場合はそのままでいい
                field.easy_data = field.data

    def get_pairs(self):
        """ キーと値(list)のタプルを返すイテレータ """
        for x in self:
            val = []
            if x.data:
                if isinstance(x.data, list):
                    for xx in x.data:
                        val.append(str(xx))
                else:
                    val.append(str(x.data))
            else:
                val.append('')

            yield (x.name, val)

    @classmethod
    def create_choices(cls, items, empty_option=None):
        """ キーがint型のdictから、タプルの選択肢を作る """
        ret = [(int(x[0]), x[1]) for x in items.items()]
        if empty_option:
            return [(-1, str(empty_option))] + ret
        else:
            return ret


class MyTranslations(object):
    def gettext(self, string):
        if string == 'Not a valid integer value':
            return '数字を入力してください'
        return string

    def ngettext(self, singular, plural, n):
        return None


class CriticalValidationError(Exception):
    def __init__(self, message):
        self.message = message
