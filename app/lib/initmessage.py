# -*- coding: utf-8 -*-
from flask import request


def set(message):
    session = request.environ['beaker.session']
    session['init_message'] = message


def get():
    session = request.environ['beaker.session']
    init_message = session.get('init_message', None)
    if init_message:
        session['init_message'] = None  # 一回参照したら消す

    return init_message
