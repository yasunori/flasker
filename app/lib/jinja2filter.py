# -*- coding: utf-8 -*-

def set_filters(app):
    app.jinja_env.filters['date_format'] = date_format_filter
    app.jinja_env.filters['nl2br'] = nl2br_filter
    app.jinja_env.filters['number_format'] = number_format_filter


def date_format_filter(value, format='%Y-%m-%d %H:%M'):
    if value:
        return value.strftime(format)


def nl2br_filter(value):
    if isinstance(value, str):
        return value.replace("\n", "<br />\n")
    return value


def number_format_filter(value):
    from babel.numbers import format_decimal
    return format_decimal(value, locale='ja_JP')

