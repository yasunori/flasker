from envelopes import Envelope, SMTP
import envelopes.connstack
from config import setting

def send_mail(from_addr, to_addr, subject, text_body, cc_addr=None, bcc_addr=None, html_body=None, headers=None, charset='utf-8', attachements=None):
    envelope = Envelope(
        from_addr=from_addr,
        to_addr=to_addr,
        subject=subject,
        text_body=text_body,
        cc_addr=cc_addr,
        bcc_addr=bcc_addr,
        html_body=html_body,
        charset=charset
        ) 

    if attachements is not None:
        for attachement in attachements:
            envelope.add_attachement(attachement)


    conn = SMTP(setting['smtp_host'], setting['smtp_port'])
    envelopes.connstack.push_connection(conn)
    smtp = envelopes.connstack.get_current_connection()
    smtp.send(envelope)
    envelopes.connstack.pop_connection()


