# -*- coding: utf-8 -*-
from sqlalchemy import text


class ModelManager:

    def __init__(self):
        from app.model import db_session
        self.db_session = db_session

    def commit(self):
        self.db_session.commit()

    def insert(self, dt):
        try:
            if(isinstance(dt, dict)):  # 辞書が来たらmodel作成
                class_f = self.get_model()
                m = class_f(**dt)
            else:
                m = dt  # modelが来たとする。
            self.db_session.add(m)
            return m
        except Exception as e:
            raise DBException() from e

    def add(self, obj):
        return self.insert(obj)

    def select_one(self, dt, key='id'):
        def is_numeric(n):
            return not isinstance(n, bool) and isinstance(n, (int, float, complex))

        try:
            class_f = self.get_model()
            if is_numeric(dt):
                dt = str(dt)
            else:
                dt = "'" + str(dt) + "'"
            f = text(key + '=' + dt)
            ret = self.db_session.query(class_f).filter(f).first()
            return self.post_select(ret)
        except Exception as e:
            raise DBException() from e

    def select(self, where=True, order_by='id', offset=0, limit=20):
        try:
            class_f = self.get_model()
            ret = self.db_session.query(class_f).filter(where).order_by(order_by)[offset:offset + limit]
            return self.post_select(ret)
        except Exception as e:
            raise DBException() from e

    def select_count(self, where=True):
        try:
            class_f = self.get_model()
            return self.db_session.query(class_f).filter(where).count()
        except Exception as e:
            raise DBException() from e

    def update(self, dt, where=True):
        try:
            class_f = self.get_model()
            ret = self.db_session.query(class_f).filter(where)
            if(ret.count() <= 0):
                return False
            for v in ret:
                for k2, v2 in dt.items():
                    setattr(v, k2, v2)
            return True
        except Exception as e:
            raise DBException() from e

    def delete(self, where=True):
        if(where is True):
            return False
        try:
            class_f = self.get_model()
            self.db_session.query(class_f).filter(where).delete()
        except Exception as e:
            raise DBException() from e

    def post_select(self, objs):
        return objs


class TransactionException(Exception):
    EXCEPTION_TYPE_ABORT = 1
    EXCEPTION_TYPE_CONTINUE = 2

    def __init__(self, message, exception_type=1, action_data=None):
        Exception.__init__(self, message)
        self.exception_type = exception_type
        self.action_data = action_data


class DBException(Exception):
    """ Exceptopn """
