# -*- coding: utf-8 -*-
import copy
import yaml
import sys
import os
from collections import OrderedDict
sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../../')
from config import BASE_DIR_NAME

MASTER_FILE = BASE_DIR_NAME + 'app/etc/master.yml'
MASTER_FILE_DEVELOPMENT = BASE_DIR_NAME + 'app/etc/master_development.yml'
MASTER_FILE_TESTING = BASE_DIR_NAME + 'app/etc/master_testing.yml'
MASTER_FILE_PRODUCTION = BASE_DIR_NAME + 'app/etc/master_production.yml'
META_FILE = BASE_DIR_NAME + 'app/etc/meta.yml'

master_data = None
meta_data = None


# OrderedDictを読み込むための関数
def construct_odict(loader, node):
    return OrderedDict(loader.construct_pairs(node))


def get(key):
    global master_data
    if not master_data:
        from config import MODE
        master_data = __read_yaml(MASTER_FILE)
        additional_file_name = None
        if MODE == 'DEVELOPMENT':
            additional_file_name = MASTER_FILE_DEVELOPMENT
        elif MODE == 'TESTING':
            additional_file_name = MASTER_FILE_TESTING
        elif MODE == 'PRODUCTION':
            additional_file_name = MASTER_FILE_PRODUCTION

        if additional_file_name:
            additional_data = __read_yaml(additional_file_name)
            master_data.update(additional_data)

    return __get(master_data, key)

def __read_yaml(filename):
    yaml.add_constructor(u'tag:yaml.org,2002:map', construct_odict)
    f = open(filename, 'r', encoding='UTF-8')
    dt = f.read()
    data = yaml.load(dt)
    f.close()
    return data


def get_meta(key):
    global meta_data
    if not meta_data:
        yaml.add_constructor(u'tag:yaml.org,2002:map', construct_odict)
        f = open(META_FILE, 'r', encoding='UTF-8')
        dt = f.read()
        meta_data = yaml.load(dt)
        f.close()
    val = meta_data.get(key, None)
    return copy.copy(val)


def __get(master_data, key):
    ret = master_data.get(key, None)
    if not ret:  # 無ければ返す
        return ret

    return ret  # そのまま返す
