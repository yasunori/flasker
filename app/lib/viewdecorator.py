from functools import wraps
from flask import request, g, abort
from app.lib.manager import DBException, TransactionException

def catch_exceptions(f):       
    """ 基本的な例外処理 """   
    @wraps(f)
    def _(*args, **kwargs):    
        try:
            return f(*args, **kwargs)   
        except DBException:    
            import logging     
            import traceback   
            logging.error(traceback.format_exc())
            abort(500)         
        except TransactionException:        
            import logging     
            import traceback   
            logging.error(traceback.format_exc())
            abort(500)
    return _
 

