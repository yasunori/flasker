from sqlalchemy.orm import relation
from sqlalchemy import Column, Integer, Text, DateTime, SmallInteger, func, ForeignKey, Boolean, Date
from app.model import Base
from app.lib.manager import ModelManager

class UsersManager(ModelManager):
    def get_model(self):
        return Users

class Users(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    name = Column(Text)
    shop_name = Column(Text)
    mail_address = Column(Text)
    password = Column(Text)
    status = Column(SmallInteger, server_default='1')
    created_at = Column(DateTime, server_default=func.now())
    updated_at = Column(DateTime, server_default=func.now(), onupdate=func.current_timestamp()) 

    user_setting = relation('UserSettings', uselist=False, backref='user')

    manager = UsersManager()
