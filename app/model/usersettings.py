from sqlalchemy.orm import relation
from sqlalchemy import Column, Integer, Text, DateTime, SmallInteger, func, ForeignKey, Boolean, Date
from app.model import Base
from app.lib.manager import ModelManager

class UserSettingsManager(ModelManager):
    def get_model(self):
        return UserSettings
    

class UserSettings(Base):
    __tablename__ = 'user_settings'
    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    type = Column(SmallInteger, server_default='99')
    free_limit_date = Column(Date)
    status = Column(SmallInteger, server_default='1')
    created_at = Column(DateTime, server_default=func.now())
    updated_at = Column(DateTime, server_default=func.now(), onupdate=func.current_timestamp())

    manager = UserSettingsManager()
