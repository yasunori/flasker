from flask import render_template, request, redirect
from flask.ext.classy import FlaskView, route
from app.lib.viewdecorator import catch_exceptions

class RootView(FlaskView):

    decorators=[catch_exceptions]

    def index(self):
        return render_template('root/index.haml')

    @route('/ad.htm')
    def ad(self):
        return render_template('root/ad.haml');

    @route('/mail.htm')
    def mail(self):
        from app.lib import mail        
        mail.send_mail(from_addr=('admin@test.jp', 'testman'),
                       to_addr=('yasunori.gotoh@gmail.com', 'yasunori'),
                       subject='てすと',
                       text_body='てすとだよ',
                       html_body='<h1>てすとだよ<h1>')
        return render_template('root/ad.haml');


    @route('/ad/<int:step>.htm')
    def ad_step(self, step):
        return render_template('root/ad.haml');

    @route('/login.htm', methods=['GET','POST'])
    def login(self):
        from app.form.sample import UserLoginForm

        if request.method == 'POST':
            form = UserLoginForm.create(request.form)
            varet = form.validate()
            if not varet:
                return render_template('root/login.haml', form=form)

            session = request.environ['beaker.session']
            session['user_id'] = 1
            return redirect('/user/')

        else:
            form = UserLoginForm.create()
            return render_template('root/login.haml', form=form);


