from functools import wraps
from flask import render_template, g, request, redirect
from flask.ext.classy import FlaskView
from app.lib.viewdecorator import catch_exceptions
from app.model.users import Users

def check_user_login(f):       
    """ログイン状態か調べる"""
    @wraps(f)
    def _(*args, **kwargs):
        session = request.environ['beaker.session']
        if session.get('user_id', None) is None:
            return redirect('/')      
        # あとでも使うはずなのでgに入れておく
        user_id = session['user_id']    
        user = Users.manager.select_one(user_id)
        g.user = user          
        print(user.name)

        return f(*args, **kwargs)       
    return _


class UserView(FlaskView):

    decorators = [catch_exceptions, check_user_login]

    def index(self):
        print('a')
        return render_template('user/index.haml')

    def get(self, id):
        return render_template('user/detail.haml')
