#!/bin/bash
export ENV_NAME=flasker     
export VIRTUALENV_PATH=/home/yasunori/venvs/$ENV_NAME
source $VIRTUALENV_PATH/bin/activate

current_dir=$(cd $(dirname $0); pwd)
batch_name=$1              
args=${@:2}                

python $current_dir/$batch_name $args
