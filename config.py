# -*- coding: utf-8 -*-
import logging
import configparser
from os import path

BASE_DIR_NAME = path.dirname(path.abspath(__file__)) + '/'
UPLOAD_DIR_NAME = BASE_DIR_NAME + 'upload/'
MODE = 'DEVELOPMENT'
setting = {}


def __read_parser_and_overwrite_setting(filename):
    global setting
    config = configparser.ConfigParser()
    config.read(filename)
    for section in config.sections():
        for k, v in config[section].items():
            setting[k] = v


def set_mode(mode):
    global MODE
    MODE = mode
    __read_parser_and_overwrite_setting(BASE_DIR_NAME + 'app/conf/config.ini')
    if mode == 'DEVELOPMENT':
        __read_parser_and_overwrite_setting(BASE_DIR_NAME + 'app/conf/config_development.ini')
    elif mode == 'TESTING':
        __read_parser_and_overwrite_setting(BASE_DIR_NAME + 'app/conf/config_testing.ini')
    elif mode == 'PRODUCTION':
        __read_parser_and_overwrite_setting(BASE_DIR_NAME + 'app/conf/config_production.ini')


def __get_app_conf(mode):
    """ Flask用のconf作る """
    config = {}

    config = {
        'DEBUG': False,
        'TESTING': False,
        'WTF_CSRF_ENABLED': False,
        'SESSION_OPTIONS': {
            'session.type': 'file',
            'session.data_dir': BASE_DIR_NAME + 'tmp',
            'session.cookie_expires': 86400 * 365,
            'session.auto': True
        },
        'DB': {
            'DSN': setting['dsn'],
            'AUTO_COMMIT': False,
            'AUTO_FLUSH': True
        },
        'DOMAIN': setting['domain'],

        'BASE_DIR_NAME': BASE_DIR_NAME,
        'UPLOAD_DIR_NAME': UPLOAD_DIR_NAME,
        'MAX_CONTENT_LENGTH': 16 * 1024 * 1024,  # 16MB
    }

    if(mode == 'PRODUCTION'):
        pass

    if(mode == 'DEVELOPMENT'):
        config['DEBUG'] = True

    if(mode == 'TESTING'):
        config['TESTING'] = True

    return config


def __set_log_conf(mode):
    LOGGING_STERAM_FLG = False
    LOGGING_STERAM_LEVEL = logging.CRITICAL
    LOGGING_FILE_FLG = True
    LOGGING_FILE_LEVEL = logging.INFO

    if mode == 'PRODUCTION':
        pass

    if mode == 'DEVELOPMENT':
        LOGGING_STERAM_FLG = True
        LOGGING_STERAM_LEVEL = logging.CRITICAL
        LOGGING_FILE_FLG = True
        LOGGING_FILE_LEVEL = logging.DEBUG

    if mode == 'TESTING':
        LOGGING_STERAM_FLG = False
        LOGGING_STERAM_LEVEL = logging.CRITICAL
        LOGGING_FILE_FLG = True
        LOGGING_FILE_LEVEL = logging.DEBUG

    '''
    ログの設定
    app.logging は使わない方針。appコンテキスト外でもログ出すので
    '''
    LOGGING_FILE_NAME_INFO = path.dirname(path.abspath(__file__)) + '/log/info.log'  # debug, info
    LOGGING_FILE_NAME_ERROR = path.dirname(path.abspath(__file__)) + '/log/error.log'  # warning, error, critical
    LOGGING_FORMAT = '%(asctime)s %(levelname)s %(process)d %(pathname)s %(lineno)d %(message)s'

    if LOGGING_STERAM_FLG:
        stream_log = logging.StreamHandler()
        stream_log.setLevel(LOGGING_STERAM_LEVEL)
        stream_log.setFormatter(logging.Formatter(LOGGING_FORMAT))
        logging.getLogger().addHandler(stream_log)

    if LOGGING_FILE_FLG:
        file_log_info = logging.FileHandler(filename=LOGGING_FILE_NAME_INFO, encoding='utf-8')
        file_log_info.setLevel(LOGGING_FILE_LEVEL)  # info より大きければ、永遠に空ファイルである
        file_log_info.setFormatter(logging.Formatter(LOGGING_FORMAT))
        logging.getLogger().addHandler(file_log_info)

        file_log_error = logging.FileHandler(filename=LOGGING_FILE_NAME_ERROR, encoding='utf-8')
        file_log_error.setLevel(logging.WARNING)  # warning以上を出す。これが出たらアウトー！
        file_log_error.setFormatter(logging.Formatter(LOGGING_FORMAT))
        logging.getLogger().addHandler(file_log_error)

    #rootロガーのログレベル（最低にしておく)
    logging.getLogger().setLevel(logging.DEBUG)


def create_app(mode=None, consider_environ=True, config=None, webapp=True):

    import os
    # 環境変数にMODEがあればそれにする
    if consider_environ:
        if os.environ.get('MODE', None):
            mode = os.environ.get('MODE')
            set_mode(mode) # global上書き

    if webapp:
        from flask import Flask
        from werkzeug import ImmutableDict
        # hamlish_jinja を使ったFlask
        class FlaskWithHamlish(Flask):
            jinja_options = ImmutableDict(extensions = [
                'jinja2.ext.autoescape',
                'jinja2.ext.with_',
                'hamlish_jinja.HamlishExtension'
            ])

        app = FlaskWithHamlish(__name__, template_folder='app/template/ja', static_folder='app/static')

        if mode:
            app.config.update(__get_app_conf(mode))

        if config:
            app.config.update(config)
    else:
        class Object: pass
        app = Object()

        if mode:
            app.config = __get_app_conf(mode)

        if config:
            app.config = config


    # ログの設定
    __set_log_conf(MODE)

    if webapp:
        # jinja2のフィルタ
        from app.lib import jinja2filter
        jinja2filter.set_filters(app)

        # 各機能を登録
        import route
        route.register_route(app)

        # エラーハンドラ
        from app.lib import errorhandler
        errorhandler.set_error_handler(app)

        # flaskのフィルタ
        from app.lib import flaskfilter
        flaskfilter.set_filters(app)

        # デバイス登録
        #import flask_devices
        #devices = flask_devices.Devices(app)
        #devices.add_pattern('sp', 'iPhone|iPod|Android.*Mobile|Windows.*Phone|dream|blackberry|CUPCAKE|webOS|incognito|webmate', config.BASE_DIR_NAME + 'webapp/templates/sp/')
        #devices.add_pattern('pc', '.*', config.BASE_DIR_NAME + 'webapp/templates/pc/')

        # セッション
        from beaker.middleware import SessionMiddleware
        app.wsgi_app = SessionMiddleware(app.wsgi_app, __get_app_conf(MODE)['SESSION_OPTIONS'])

    return app


# 初期化
set_mode(MODE)

