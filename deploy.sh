#!/bin/bash
export ENV_NAME=flasker
export VIRTUALENV_PATH=/Users/yasunori/venvs/$ENV_NAME
source $VIRTUALENV_PATH/bin/activate

cd /var/app/flasker
git pull
alembic upgrade head

touch .uwsgi_touch
touch .uwsgi_touch
touch .uwsgi_touch
touch .uwsgi_touch
