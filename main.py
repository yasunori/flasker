# -*- coding: utf-8 -*-
import os
import config

mode = 'DEVELOPMENT'

if __name__ == '__main__':
    # 直接起動は開発
    mode = 'DEVELOPMENT'
else:
    mode = 'PRODUCTION'

# App作成
config.set_mode(mode)
app = config.create_app(config.MODE)

if __name__ == '__main__':
    app.run(host='0.0.0.0')
