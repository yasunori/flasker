# -*- coding: utf-8 -*-


def register_route(app):

    from app.view.root import RootView
    from app.view.user import UserView

    RootView.register(app, route_base='/') # route_base を指定しないと /root/ になる
    UserView.register(app)

    # uploadディレクトリ
    #from werkzeug import SharedDataMiddleware
    #app.add_url_rule('/upload/<filename>', 'uploaded_file', build_only=True)
    #app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {'/upload': app.config['UPLOAD_DIR_NAME']})
